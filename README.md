gizbob
======

A small programming language. Just for fun. Learning a lot!

## Syntax

```gizbob
fib : {[terms]
    t1 : 0
    t2 : 1
    i : 1
    nextTerm : 0
    iter : {[]
        printf("%.f\n", nextTerm)
        t1 = t2
        t2 = nextTerm
        nextTerm = +(t1, t2)
        if(<(i, terms),
            {[]
                i = +(i, 1)
                iter()
            }
        )
    }
    iter()
}

fib(200)
```
