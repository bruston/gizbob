package main

import (
	"fmt"
	"os"

	"github.com/bruston/gizbob/parser"
	"github.com/bruston/gizbob/runtime"
)

func main() {
	if len(os.Args) < 2 {
		fmt.Printf("Missing file name to run as argument: gizbob foo.giz\n")
		return
	}
	source, err := os.Open(os.Args[1])
	if err != nil {
		fmt.Fprintf(os.Stderr, "unable to open source file: %s", err)
		return
	}
	defer source.Close()
	p := parser.New(source)
	tree, err := p.Parse()
	if err != nil {
		fmt.Fprintf(os.Stderr, "error parsing source: %s", err)
		return
	}
	scope := runtime.NewGlobalScope()
	for _, v := range tree {
		runtime.Eval(v, scope)
	}
}
