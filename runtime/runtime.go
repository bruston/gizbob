package runtime

import (
	"log"

	"github.com/bruston/gizbob/parser"
)

type ValueType int

const (
	StringValue ValueType = iota
	NumValue
	FuncValue
	ArrayValue
	BoolValue
	NilValue
)

func (v ValueType) Type() ValueType { return v }

type Value interface {
	Type() ValueType
	Value() interface{}
}

type ValueString struct {
	ValueType
	Val string
}

func (v ValueString) Value() interface{} { return v.Val }

type ValueNum struct {
	ValueType
	Val float64
}

func (v ValueNum) Value() interface{} { return v.Val }

type ValueFunc struct {
	ValueType
	Params []string
	Body   []parser.Expr
	Native func(scope *Scope, args ...Value) Value
}

func (v ValueFunc) Value() interface{} { return v }

type ValueArray struct {
	ValueType
	Elements []Value
}

type ValueBool struct {
	ValueType
	Val bool
}

func (v ValueBool) Value() interface{} { return v.Val }

func (v ValueArray) Value() interface{} { return v.Elements }

type ValueNil struct {
	ValueType
}

func (v ValueNil) Value() interface{} { return nil }

type ValueObject struct {
	ValueType
	Fields map[string]Value
}

func (v ValueObject) Value() interface{} { return v.Fields }

type Scope struct {
	parent          *Scope
	vars            map[string]Value
	lastAddressable Value // the last function literal or array evaluated
}

func NewGlobalScope() *Scope {
	scope := NewScope(nil)
	for k, v := range builtin {
		scope.Set(k, ValueFunc{ValueType: FuncValue, Native: v})
	}
	return scope
}

func NewScope(parent *Scope) *Scope {
	return &Scope{
		parent: parent,
		vars:   make(map[string]Value),
	}
}

func (e *Scope) Lookup(name string) (Value, bool) {
	if v, ok := e.vars[name]; ok {
		return v, ok
	}
	if e.parent != nil {
		return e.parent.Lookup(name)
	}
	return nil, false
}

func (e *Scope) Find(name string) *Scope {
	if _, ok := e.vars[name]; ok {
		return e
	}
	if e.parent != nil {
		return e.parent.Find(name)
	}
	return nil
}

func (e *Scope) Set(name string, value Value) {
	e.vars[name] = value
}

func Eval(exp parser.Expr, scope *Scope) Value {
	switch n := exp.(type) {
	case parser.StringLitExpr:
		return ValueString{StringValue, n.Value}
	case parser.NumLitExpr:
		return ValueNum{NumValue, n.Value}
	case parser.ArrayLitExpr:
		a := ValueArray{ValueType: ArrayValue}
		for _, elem := range n.Elements {
			a.Elements = append(a.Elements, Eval(elem, scope))
		}
		return a
	case parser.SymbolExpr:
		v, _ := scope.Lookup(n.Name)
		if n.Index == nil {
			return v
		}
		arr, ok := v.(ValueArray)
		if !ok {
			return nil
		}
		index, ok := Eval(n.Index, scope).(ValueNum)
		if !ok {
			return nil
		}
		return arr.Elements[int(index.Val)]
	case parser.FuncLitExpr:
		fn := ValueFunc{ValueType: FuncValue, Params: n.Params, Body: n.Body}
		scope.lastAddressable = fn
		return fn
	case parser.DeclarationExpr:
		v := EvalMany(n.Right, scope)
		scope.Set(n.Left.Name, v)
		return v
	case parser.AssignmentExpr:
		sc := scope.Find(n.Left.Name)
		if sc == nil {
			log.Printf("%s was not found in scope", n.Left.Name)
			return nil
		}
		v := EvalMany(n.Right, scope)
		if n.Left.Index == nil {
			sc.Set(n.Left.Name, v)
			return v
		}
		index, _ := Eval(n.Left.Index, scope).(ValueNum)
		val := sc.vars[n.Left.Name]
		arr, _ := val.(ValueArray)
		arr.Elements[int(index.Val)] = v
		return v
	case parser.FuncCallExpr:
		scope := NewScope(scope)
		v, ok := scope.Lookup(n.Name)
		if !ok {
			return v
		}
		fn, _ := v.(ValueFunc)
		ret := call(scope, fn, n.Params, fn.Params)
		if isFn, ok := ret.(ValueFunc); ok {
			scope.parent.lastAddressable = isFn
		}
		return ret
	case parser.AnonFuncCallExpr:
		fn, ok := scope.lastAddressable.(ValueFunc)
		if !ok {
			return ValueNil{NilValue}
		}
		scope := NewScope(scope)
		ret := call(scope, fn, n.Params, fn.Params)
		if isFn, ok := ret.(ValueFunc); ok {
			scope.parent.lastAddressable = isFn
		}
		return ret
	}
	return ValueNil{NilValue}
}

func call(scope *Scope, fn ValueFunc, args []parser.Expr, symbols []string) Value {
	var params []Value
	for i, param := range args {
		v := Eval(param, scope)
		params = append(params, v)
		if fn.Native == nil {
			scope.Set(symbols[i], v)
		}
	}
	if fn.Native != nil {
		return fn.Native(scope, params...)
	}
	var last Value
	for _, expr := range fn.Body {
		last = Eval(expr, scope)
	}
	return last
}

func EvalMany(expressions []parser.Expr, scope *Scope) Value {
	var last Value
	for _, exp := range expressions {
		last = Eval(exp, scope)
	}
	return last
}
