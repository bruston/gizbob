package runtime

import (
	"errors"
	"fmt"

	"github.com/bruston/gizbob/parser"
)

func doBinOp(args []Value, opFn func(a, b ValueNum) Value) (Value, error) {
	if len(args) < 2 {
		return nil, errors.New("not enough arguments to perform binary operation")
	}
	a, aOk := args[0].(ValueNum)
	b, bOk := args[1].(ValueNum)
	if !aOk || !bOk {
		return nil, errors.New("attempted binary operation on non number types")
	}
	return opFn(a, b), nil
}

var builtin = map[string]func(scope *Scope, args ...Value) Value{
	"printf": func(e *Scope, args ...Value) Value {
		if len(args) < 1 {
			return ValueNil{NilValue}
		}
		fs := args[0].(ValueString).Val
		if len(args) == 1 {
			fmt.Printf(fs)
			return ValueNil{NilValue}
		} else if len(args) == 2 {
			fmt.Printf(fs, args[1].Value())
			return ValueNil{NilValue}
		}
		params := make([]interface{}, len(args[1:]))
		for i, v := range args[1:] {
			params[i] = v.Value()
		}
		fmt.Printf(fs, params...)
		return ValueNil{NilValue}
	},
	"true": func(e *Scope, args ...Value) Value {
		return ValueBool{BoolValue, true}
	},
	"false": func(e *Scope, args ...Value) Value {
		return ValueBool{BoolValue, false}
	},
	"if": func(e *Scope, args ...Value) Value {
		if len(args) < 2 {
			return nil
		}
		var cond bool
		switch v := args[0].(type) {
		case ValueBool:
			cond = v.Val
		case ValueFunc:
			ret := EvalMany(v.Body, e)
			b, ok := ret.(ValueBool)
			if !ok {
				return nil
			}
			cond = b.Val
		}
		ifBody := args[1].(ValueFunc).Body
		var elseBody []parser.Expr
		if len(args) > 2 {
			elseBody = args[2].(ValueFunc).Body
		}
		var last Value
		if cond {
			for _, exp := range ifBody {
				last = Eval(exp, e)
			}
		} else {
			if elseBody != nil {
				for _, exp := range elseBody {
					last = Eval(exp, e)
				}
			}
		}
		return last
	},
	"+": func(e *Scope, args ...Value) Value {
		v, err := doBinOp(args, func(a, b ValueNum) Value {
			return ValueNum{NumValue, a.Val + b.Val}
		})
		if err != nil {
			return ValueNil{NilValue}
		}
		return v
	},
	"-": func(e *Scope, args ...Value) Value {
		v, err := doBinOp(args, func(a, b ValueNum) Value {
			return ValueNum{NumValue, a.Val - b.Val}
		})
		if err != nil {
			return ValueNil{NilValue}
		}
		return v
	},
	"*": func(e *Scope, args ...Value) Value {
		v, err := doBinOp(args, func(a, b ValueNum) Value {
			return ValueNum{NumValue, a.Val * b.Val}
		})
		if err != nil {
			return ValueNil{NilValue}
		}
		return v

	},
	"/": func(e *Scope, args ...Value) Value {
		v, err := doBinOp(args, func(a, b ValueNum) Value {
			return ValueNum{NumValue, a.Val / b.Val}
		})
		if err != nil {
			return ValueNil{NilValue}
		}
		return v

	},
	"mod": func(e *Scope, args ...Value) Value {
		v, err := doBinOp(args, func(a, b ValueNum) Value {
			return ValueNum{NumValue, float64(int(a.Val) % int(b.Val))}
		})
		if err != nil {
			return ValueNil{NilValue}
		}
		return v

	},
	"<": func(e *Scope, args ...Value) Value {
		v, err := doBinOp(args, func(a, b ValueNum) Value {
			return ValueBool{BoolValue, a.Val < b.Val}
		})
		if err != nil {
			return ValueNil{NilValue}
		}
		return v

	},
	">": func(e *Scope, args ...Value) Value {
		v, err := doBinOp(args, func(a, b ValueNum) Value {
			return ValueBool{BoolValue, a.Val > b.Val}
		})
		if err != nil {
			return ValueNil{NilValue}
		}
		return v
	},
	"len": func(e *Scope, args ...Value) Value {
		if len(args) < 1 {
			return ValueNil{NilValue}
		}
		if args[0].Type() == StringValue {
			return ValueNum{NumValue, float64(len(args[0].Value().(string)))}
		}
		if args[0].Type() == ArrayValue {
			return ValueNum{NumValue, float64(len(args[0].Value().([]Value)))}
		}
		return ValueNil{NilValue}
	},
	"equal": func(e *Scope, args ...Value) Value {
		eq := ValueBool{BoolValue, false}
		if len(args) < 2 || args[0].Type() != args[1].Type() {
			return eq
		}
		eq.Val = args[0].Value() == args[1].Value()
		return eq
	},
	"array": func(e *Scope, args ...Value) Value {
		return nil
	},
	"append": func(e *Scope, args ...Value) Value {
		return nil
	},
	"object": func(e *Scope, args ...Value) Value {
		return nil
	},
}
