package runtime

import (
	"strings"
	"testing"

	"github.com/bruston/gizbob/parser"
)

func TestRuntime(t *testing.T) {
	input := "foo : {[] 15} printf(\"%.f\n\", foo())"
	p := parser.New(strings.NewReader(input))
	tree, err := p.Parse()
	if err != nil {
		t.Fatal(err)
	}
	scope := NewGlobalScope()
	for _, exp := range tree {
		Eval(exp, scope)
	}
}

func TestMore(t *testing.T) {}
