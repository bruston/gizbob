package parser

import (
	"fmt"
	"io"
	"strconv"

	"github.com/bruston/gizbob/lexer"
)

type ExpressionType int

type Expr interface {
	Expr() ExpressionType
}

const (
	ExpSymbol ExpressionType = iota
	ExpAssignment
	ExpDeclaration
	ExpFuncLit
	ExpFuncCall
	ExpArrayLit
	ExpStringLit
	ExpNumLit
	ExpAnonFuncCall
)

func (e ExpressionType) Expr() ExpressionType { return e }

type AnonFuncCallExpr struct {
	ExpressionType
	Params []Expr
}

type SymbolExpr struct {
	ExpressionType
	Name  string
	Index Expr
}

type AssignmentExpr struct {
	ExpressionType
	Left  SymbolExpr
	Right []Expr
}

type DeclarationExpr struct {
	ExpressionType
	Left  SymbolExpr
	Right []Expr
}

type FuncLitExpr struct {
	ExpressionType
	Params []string
	Body   []Expr
}

type FuncCallExpr struct {
	ExpressionType
	Name   string
	Params []Expr
}

type ArrayLitExpr struct {
	ExpressionType
	Elements []Expr
}

type StringLitExpr struct {
	ExpressionType
	Value string
}

type NumLitExpr struct {
	ExpressionType
	Value float64
}

type Parser struct {
	lex *lexer.Lexer
	ast []Expr
}

func New(r io.Reader) *Parser {
	return &Parser{lex: lexer.NewLexer(r)}
}

func (p *Parser) Parse() ([]Expr, error) {
	var expressions []Expr
	for {
		expr, err := p.nextExpr()
		if err != nil {
			return nil, err
		}
		if expr == nil {
			break
		}
		expressions = append(expressions, expr)
	}
	return expressions, nil
}

func (p *Parser) nextExpr() (Expr, error) {
	item := p.lex.Item()
	if item.Type == lexer.ItemEOF {
		return nil, nil
	}
	switch item.Type {
	case lexer.ItemNumLit:
		n, err := strconv.ParseFloat(item.Value, 64)
		if err != nil {
			return nil, fmt.Errorf("unable to parse float at line %d column %d: %s", item.Line, item.Pos, err)
		}
		return NumLitExpr{ExpressionType: ExpNumLit, Value: n}, nil
	case lexer.ItemStringLit:
		return StringLitExpr{ExpressionType: ExpStringLit, Value: item.Value}, nil
	case lexer.ItemSymbol:
		s := SymbolExpr{
			ExpressionType: ExpSymbol,
			Name:           item.Value,
		}
		// If next token is an opening square bracket, it's a symbol
		// with an index.
		next := p.lex.Peek()
		if next.Type == lexer.ItemOpenSquare {
			p.lex.Item()
			index, err := p.nextExpr()
			if err != nil {
				return nil, err
			}
			if term := p.lex.Peek(); term.Type != lexer.ItemCloseSquare {
				return nil, fmt.Errorf("expecting closing square bracket at line %d pos %d got %s instead",
					term.Line, term.Pos, term.Value)
			}
			p.lex.Item()
			s.Index = index
		}
		// Assign
		if p.lex.Peek().Type == lexer.ItemAssign {
			p.lex.Item()
			next, err := p.nextExpr()
			if err != nil {
				return nil, err
			}
			a := AssignmentExpr{ExpressionType: ExpAssignment, Left: s, Right: []Expr{next}}
			for {
				if p.lex.Peek().Type != lexer.ItemOpenParen {
					break
				}
				next, err := p.nextExpr()
				if err != nil {
					return nil, err
				}
				a.Right = append(a.Right, next)
			}
			return a, nil
		}
		// Declaration TODO: remove duplication
		if p.lex.Peek().Type == lexer.ItemDeclaration {
			p.lex.Item()
			next, err := p.nextExpr()
			if err != nil {
				return nil, err
			}
			a := DeclarationExpr{ExpressionType: ExpAssignment, Left: s, Right: []Expr{next}}
			for {
				if p.lex.Peek().Type != lexer.ItemOpenParen {
					break
				}
				next, err := p.nextExpr()
				if err != nil {
					return nil, err
				}
				a.Right = append(a.Right, next)
			}
			return a, nil
		}

		// Function call
		if next.Type == lexer.ItemOpenParen {
			p.lex.Item()
			params, err := p.getMultiExpr(lexer.ItemComma, lexer.ItemCloseParen)
			if err != nil {
				return nil, err
			}
			return FuncCallExpr{ExpressionType: ExpFuncCall, Name: s.Name, Params: params}, nil
		}
		// Else it's a plain symbol expression
		return s, nil
		// Function literal
	case lexer.ItemOpenBrace:
		var (
			params []Expr
			body   []Expr
			err    error
		)
		if next := p.lex.Peek(); next.Type != lexer.ItemOpenSquare {
			return nil, fmt.Errorf("expecting parameter list in function literal")
		}
		p.lex.Item()
		if params, err = p.getMultiExpr(lexer.ItemComma, lexer.ItemCloseSquare); err != nil {
			return nil, err
		}
		if body, err = p.getMultiExpr(lexer.ItemSemiColon, lexer.ItemCloseBrace); err != nil {
			return nil, err
		}
		return FuncLitExpr{ExpressionType: ExpFuncLit, Params: symbolsFromMany(params), Body: body}, nil
	case lexer.ItemOpenSquare:
		elements, err := p.getMultiExpr(lexer.ItemComma, lexer.ItemCloseSquare)
		if err != nil {
			return nil, err
		}
		return ArrayLitExpr{ExpressionType: ExpArrayLit, Elements: elements}, nil
	case lexer.ItemOpenParen:
		params, err := p.getMultiExpr(lexer.ItemComma, lexer.ItemCloseParen)
		if err != nil {
			return nil, err
		}
		return AnonFuncCallExpr{ExpressionType: ExpAnonFuncCall, Params: params}, nil
	}
	return nil, nil
}

func symbolsFromMany(exprs []Expr) []string {
	var names []string
	for _, e := range exprs {
		s, ok := e.(SymbolExpr)
		if !ok {
			continue
		}
		names = append(names, s.Name)
	}
	return names
}

func (p *Parser) getMultiExpr(delim, end int) ([]Expr, error) {
	var body []Expr
	for {
		if next := p.lex.Peek(); next.Type == delim {
			p.lex.Item()
			continue
		} else if next := p.lex.Peek(); next.Type == end {
			p.lex.Item()
			break
		}
		exp, err := p.nextExpr()
		if err != nil {
			return nil, err
		}
		body = append(body, exp)
	}
	return body, nil
}
