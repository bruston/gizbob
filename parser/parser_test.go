package parser

import (
	"log"
	"strings"
	"testing"
)

func TestParser(t *testing.T) {
	input := `foo = {[n] printf("hello world\n"); } moo()`
	p := New(strings.NewReader(input))
	tree, err := p.Parse()
	if err != nil {
		t.Fatal(err)
	}
	for _, v := range tree {
		log.Print(v.Expr())
	}
}
