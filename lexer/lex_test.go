package lexer

import (
	"log"
	"strings"
	"testing"
)

func TestLexer(t *testing.T) {
	input := `recursive = {[n]
		if (>(n, 0), {
			printf("%d\n", n);
			recursive(-(n, 1));
		});
		printf("done\n");
}`
	var items []Item
	scanner := NewLexer(strings.NewReader(input))
	for {
		itm := scanner.Item()
		if itm.Type == ItemEOF {
			break
		}
		items = append(items, itm)
	}

	for _, v := range items {
		log.Print(v)
	}
}
