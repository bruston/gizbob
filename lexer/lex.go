package lexer

import (
	"bufio"
	"bytes"
	"io"
	"strconv"
	"unicode"
	"unicode/utf8"
)

const (
	ItemSymbol = iota
	ItemStringLit
	ItemNumLit
	ItemComma
	ItemOpenParen
	ItemCloseParen
	ItemOpenBrace
	ItemCloseBrace
	ItemOpenSquare
	ItemCloseSquare
	ItemSemiColon
	ItemAssign
	ItemDeclaration
	ItemEOF
)

type Item struct {
	Type  int
	Value string
	Line  int
	Pos   int
}

type Lexer struct {
	src     *bufio.Reader
	buf     *bytes.Buffer
	current Item
	next    Item
	last    rune
	line    int
	pos     int
	err     error
}

func NewLexer(r io.Reader) *Lexer {
	l := &Lexer{
		src:  bufio.NewReader(r),
		buf:  bytes.NewBuffer(make([]byte, 0, 1024)),
		line: 1,
	}
	l.scan()
	return l
}

func (l *Lexer) read() (rune, error) {
	ch, size, err := l.src.ReadRune()
	if err != nil {
		return 0, err
	}
	l.pos += size
	l.last = ch
	if ch == '\n' {
		l.line++
		l.pos = 0
	}
	return ch, nil
}

func (l *Lexer) unread() {
	l.src.UnreadRune()
	if l.last == '\n' {
		l.line--
	}
	l.pos -= utf8.RuneLen(l.last)
	l.last = 0
}

func (l *Lexer) peek() (rune, error) {
	b, err := l.src.Peek(1)
	return rune(b[0]), err
}

func (l *Lexer) skipSpace() {
	for {
		ch, _ := l.read()
		if !unicode.IsSpace(ch) {
			l.unread()
			break
		}
	}
}

func (l *Lexer) scanSymbol() (Item, error) {
	defer l.buf.Reset()
	line, pos := l.line, l.pos+1
	for {
		ch, err := l.read()
		if err != nil {
			if l.buf.Len() == 0 {
				return Item{}, err
			}
			l.unread()
			break
		}
		if unicode.IsSpace(ch) || ch == '(' || ch == ')' || ch == '[' || ch == ']' ||
			ch == '{' || ch == '}' || ch == ';' || ch == ',' {
			l.unread()
			break
		}
		l.buf.WriteRune(ch)
	}
	return Item{Type: ItemSymbol, Value: l.buf.String(), Line: line, Pos: pos}, nil
}

func (l *Lexer) scanNumber() (Item, error) {
	defer l.buf.Reset()
	line, pos := l.line, l.pos+1
	for {
		ch, err := l.read()
		if err != nil {
			if l.buf.Len() == 0 {
				return Item{}, err
			}
			l.unread()
			break
		}
		if !unicode.IsDigit(ch) && ch != '-' {
			l.unread()
			break
		}
		l.buf.WriteRune(ch)
	}
	return Item{Type: ItemNumLit, Value: l.buf.String(), Line: line, Pos: pos}, nil
}

func (l *Lexer) scanString() string {
	escape := false
	for {
		ch, err := l.read()
		if err != nil {
			break
		}
		if escape {
			l.buf.WriteRune(ch)
			escape = false
			continue
		}
		if ch == '\\' && !escape {
			escape = true
			l.buf.WriteRune(ch)
			continue
		}
		if ch == '"' {
			break
		}
		l.buf.WriteRune(ch)
	}
	s := l.buf.String()
	// It might contain escape sequences, so this unquote attempt
	// is required.
	if uq, err := strconv.Unquote(`"` + s + `"`); err == nil {
		s = uq
	}
	l.buf.Reset()
	return s
}

func newItem(typ int, line int, pos int, lit string) Item {
	return Item{
		Line:  line,
		Pos:   pos,
		Type:  typ,
		Value: lit,
	}
}

func (l *Lexer) scan() {
	if l.current.Type == ItemEOF {
		return
	}
	l.skipSpace()
	ch, err := l.read()
	if err != nil {
		if err == io.EOF {
			l.next = newItem(ItemEOF, l.line, l.pos, "<EOF>")
		}
		l.err = err
		return
	}
	if unicode.IsDigit(ch) {
		l.unread()
		l.next, l.err = l.scanNumber()
		return
	}
	if ch == '-' {
		if next, _ := l.peek(); !unicode.IsDigit(next) {
			l.unread()
			l.next, l.err = l.scanSymbol()
		} else {
			l.unread()
			l.next, l.err = l.scanNumber()
		}
		return
	}
	switch ch {
	case ':':
		l.next, l.err = newItem(ItemDeclaration, l.line, l.pos, ":"), nil
	case '{':
		l.next, l.err = newItem(ItemOpenBrace, l.line, l.pos, "{"), nil
	case '}':
		l.next, l.err = newItem(ItemCloseBrace, l.line, l.pos, "}"), nil
	case '(':
		l.next, l.err = newItem(ItemOpenParen, l.line, l.pos, "("), nil
	case ')':
		l.next, l.err = newItem(ItemCloseParen, l.line, l.pos, ")"), nil
	case '[':
		l.next, l.err = newItem(ItemOpenSquare, l.line, l.pos, "["), nil
	case ']':
		l.next, l.err = newItem(ItemCloseSquare, l.line, l.pos, "]"), nil
	case ';':
		l.next, l.err = newItem(ItemSemiColon, l.line, l.pos, ";"), nil
	case ',':
		l.next, l.err = newItem(ItemComma, l.line, l.pos, ","), nil
	case '=':
		l.next, l.err = newItem(ItemAssign, l.line, l.pos, "="), nil
	case '"':
		l.next, l.err = newItem(ItemStringLit, l.line, l.pos, l.scanString()), nil
	default:
		l.unread()
		l.next, l.err = l.scanSymbol()
	}
}

func (l *Lexer) Item() Item {
	l.current = l.next
	l.scan()
	return l.current
}

func (l *Lexer) Peek() Item {
	return l.next
}

func (l *Lexer) Err() error {
	if l.err == nil || l.err == io.EOF {
		return nil
	}
	return l.err
}
